﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Web_S10189272.Models
{
    public class Branch
    {
        [Required]
        [Display(Name = "ID")]
        public int BranchNo { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [RegularExpression(@"[689]\d{7}|\+65[689]\d{7}$")]
        [Required(ErrorMessage = "Invalid Singapore Phone Number")]
        [Display(Name = "Telephone")]
        public  string Telephone { get; set; } 
    }
}
