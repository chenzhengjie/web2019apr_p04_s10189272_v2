﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Web_S10189272.Models
{
    public class Vote
    {
        [Display(Name = "Book ID")]
        public int BookId { get; set; }
        [Display(Name = "Justification")]
        public string Justification { get; set; }
        [Display(Name = "Title")]
        public string Title { get; set; }
    }
}
