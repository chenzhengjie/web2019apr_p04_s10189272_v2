﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_S10189272.Models
{
    public class RentalDiscount
    {
        public string Description { get; set; }
        public double DiscountPercent { get; set; }
        public bool Selected { get; set; }
    }
}
