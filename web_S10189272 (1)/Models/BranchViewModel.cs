﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_S10189272.Models
{
    public class BranchViewModel
    {
        public List<Branch> branchList { get; set; }
        public List<Staff> staffList { get; set; }
    }
}
