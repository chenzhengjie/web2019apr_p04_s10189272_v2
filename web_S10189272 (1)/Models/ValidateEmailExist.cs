﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Web_S10189272.DAL;

namespace Web_S10189272.Models
{
    public class ValidateEmailExist : ValidationAttribute
    {
        private StaffDAL staffContext = new StaffDAL();

        public override bool IsValid(object value)
        {
            string email = Convert.ToString(value);
            if (staffContext.IsEmailExist(email))
                return false; // validation failed
            else
                return true;
        }
    }
}
