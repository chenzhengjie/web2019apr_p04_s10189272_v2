﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Web_S10189272.Models
{
    public class Staff
    {
        public int StaffId { get; set; }

        [Required]
        [StringLength (50)]
        public string Name { get; set; }
        public char Gender { get; set; }
        public DateTime DOB { get; set; }
        public string Nationality { get; set; }

        [Display(Name = "EmailAddress")]
        [EmailAddress]//ValidationAnnotationforemailaddressformat
        //CustomValidationAttributeforchecking emailaddressexists
        [ValidateEmailExist(ErrorMessage="Emailaddressalreadyexists!")]
        public string Email { get; set; }

        public decimal Salary { get; set; }
        public bool IsFullTime { get; set; }
        public int? BranchNo { get; set; }
    }
}
