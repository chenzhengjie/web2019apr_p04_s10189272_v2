﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Web_S10189272.Models; 

namespace Web_S10189272.DAL
{

    public class BranchDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        public BranchDAL()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("NPBookConnectionString");

            conn = new SqlConnection(strConn);
        }

        public List<Branch> GetAllBranches()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Branch", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "BranchDetails");
            conn.Close();

            List<Branch> branchList = new List<Branch>();
            foreach (DataRow row in result.Tables["BranchDetails"].Rows)
            {
                branchList.Add(
                    new Branch
                    {
                        BranchNo = Convert.ToInt32(row["BranchNo"]),
                        Address = row["Address"].ToString(),
                        Telephone = row["TelNo"].ToString()
                    });
            }
            return branchList;
        }

        public List<Staff> GetBranchStaff(int branchNo)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Staff WHERE BranchNo = @selectedBranch", conn);
            cmd.Parameters.AddWithValue("@selectedBranch", branchNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            da.Fill(result, "StaffDetails");
            conn.Close();
            List<Staff> staffList = new List<Staff>();
            foreach (DataRow row in result.Tables["StaffDetails"].Rows)
            {
                staffList.Add(
                    new Staff {
                        StaffId = Convert.ToInt32(row["StaffID"]),
                        Name = row["Name"].ToString(),
                        Gender = Convert.ToChar(row["Gender"]),
                        DOB = Convert.ToDateTime(row["DOB"]),
                        Salary = Convert.ToDecimal(row["Salary"]),
                        Nationality = row["Nationality"].ToString(),
                        Email = row["EmailAddr"].ToString(),
                        IsFullTime = Convert.ToBoolean(row["Status"]),
                        BranchNo = branchNo
                    });
            }
            return staffList;
        }
    }
}
