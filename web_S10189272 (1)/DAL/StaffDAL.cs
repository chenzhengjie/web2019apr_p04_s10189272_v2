﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Web_S10189272.Models;

namespace Web_S10189272.DAL
{
    public class StaffDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public StaffDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettings.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("NPBookConnectionString");

            //Instantiate a SqlConnection object with the 
            //Connection String read.  
            conn = new SqlConnection(strConn);
        }

        public List<Staff> GetAllStaff()
        {
            //Instantiate a SqlCommand object, supply it with a 
            //SELECTSQL statementthat operates against the database, 
            //and the connection objectfor connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Staff ORDER BY StaffID", conn);
            //Instantiate a DataAdapter object and pass the 
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "StaffDetails");
            conn.Close();

            //Transferringrowsof data in DataSet’s tableto “Staff” objects
            List<Staff> staffList = new List<Staff>();foreach (DataRow row in result.Tables["StaffDetails"].Rows)
            {
                int? branchNo;
                //BranchNo column not null
                if (!DBNull.Value.Equals(row["BranchNo"]))
                    branchNo = Convert.ToInt32(row["BranchNo"]);
                else
                    branchNo = null;

                staffList.Add(
                    new Staff
                    {
                        StaffId = Convert.ToInt32(row["StaffID"]),
                        Name = row["Name"].ToString(),
                        Gender = Convert.ToChar(row["Gender"]),
                        DOB = Convert.ToDateTime(row["DOB"]),
                        Salary = Convert.ToDecimal(row["Salary"]),
                        Nationality = row["Nationality"].ToString(),
                        Email = row["EmailAddr"].ToString(),
                        IsFullTime = Convert.ToBoolean(row["Status"]),
                        BranchNo = branchNo
                    }
                    );
            }
            return staffList;
        }

        public int Add(Staff staff)
        {
            //InstantiateaSqlCommandobject,supplyitwithanINSERTSQLstatement
            //whichwillreturntheauto-generatedStaffIDafterinsertion,
            //andtheconnectionobjectforconnectingtothedatabase.
            SqlCommand cmd = new SqlCommand
                             ("INSERT INTO Staff(Name,Gender,DOB,Salary," + "EmailAddr,Nationality,Status)" + 
                             "OUTPUT INSERTED.StaffID" + 
                             "VALUES(@name,@gender,@dob,@salary," + 
                             "@email,@country,@status)", conn);

            //DefinetheparametersusedinSQLstatement,valueforeachparameter
            //isretrievedfromrespectiveclass'sproperty.
            cmd.Parameters.AddWithValue("@name", staff.Name);
            cmd.Parameters.AddWithValue("@gender", staff.Gender);
            cmd.Parameters.AddWithValue("@dob", staff.DOB);
            cmd.Parameters.AddWithValue("@salary", staff.Salary);
            cmd.Parameters.AddWithValue("@email", staff.Email);
            cmd.Parameters.AddWithValue("@country", staff.Nationality);
            cmd.Parameters.AddWithValue("@status", staff.IsFullTime);

            conn.Open();
            staff.StaffId = (int)cmd.ExecuteScalar();
            conn.Close();

            return staff.StaffId;

        }

        public bool IsEmailExist(string email)
        {
            SqlCommand cmd = new SqlCommand("SELECT StaffID FROM Staff WHERE EmailAddr=@selectedEmail", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);

            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daEmail.Fill(result, "EmailDetails");
            conn.Close();

            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; //Theemailexistsforanotherstaff
            else
                return false; //Theemailaddressgivendoesnotexist
        }

        public Staff GetDetails(int staffId)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Staff WHERE StaffID = @selectedStaffID", conn);
            cmd.Parameters.AddWithValue("@selectedStaffID", staffId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "StaffDetails");
            conn.Close();

            Staff staff = new Staff();
            if (result.Tables["StaffDetails"].Rows.Count > 0)
            {
                staff.StaffId = staffId;
                DataTable table = result.Tables["StaffDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                    staff.Name = table.Rows[0]["Name"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Gender"]))
                    staff.Gender = Convert.ToChar(table.Rows[0]["Gender"]);
                if (!DBNull.Value.Equals(table.Rows[0]["DOB"]))
                    staff.DOB = Convert.ToDateTime(table.Rows[0]["DOB"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Nationality"]))
                    staff.Nationality = table.Rows[0]["Nationality"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                    staff.Email = table.Rows[0]["EmailAddr"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Salary"]))
                    staff.Salary = Convert.ToDecimal(table.Rows[0]["Salary"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Status"]))
                    staff.IsFullTime = Convert.ToBoolean(table.Rows[0]["Status"]);
                if (!DBNull.Value.Equals(table.Rows[0]["BranchNo"]))
                    staff.BranchNo = Convert.ToInt32(table.Rows[0]["BranchNo"]);
                return staff;
            }
            else
            {
                return null;
            } 
        }

        public int Update(Staff staff)
        {
            SqlCommand cmd = new SqlCommand(
                "UPDATE Staff SET Salary=@salary, Status=@status, BranchNo=@branchNo" +
                " WHERE StaffID = @selectedStaffID", conn);
            cmd.Parameters.AddWithValue("@salary", staff.Salary);
            cmd.Parameters.AddWithValue("@status", staff.IsFullTime);

            if (staff.BranchNo != null)
                cmd.Parameters.AddWithValue("@branchNo", staff.BranchNo.Value);
            else     
                cmd.Parameters.AddWithValue("@branchNo", DBNull.Value);

            cmd.Parameters.AddWithValue("@selectedStaffID", staff.StaffId);
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            return count;
        }

        public int Delete(int staffId)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM Staff " + "WHERE StaffID = @selectStaffID", conn);
            cmd.Parameters.AddWithValue("@selectStaffID", staffId);

            conn.Open();
            int rowCount;
            rowCount = cmd.ExecuteNonQuery();
            conn.Close();
            return rowCount;
        }
    }
}
