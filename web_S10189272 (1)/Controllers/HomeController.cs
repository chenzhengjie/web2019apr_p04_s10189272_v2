﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace web_S10189272__1_.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StaffLogin(IFormCollection formData)
        {
            // Read inputs from textboxes
            //Email address converted to lowercase
            string loginID = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();
            if (loginID == "abc@npbook.com" && password == "pass1234")
            {
                // Store Login ID in sessionwith thekey as “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role“Staff”in session with thekey as “Role”
                HttpContext.Session.SetString("Role", "Staff");
                // Redirect user to the "StaffMain" viewthrough an action
                return RedirectToAction("StaffMain");
            }
            else {
                // Store an error message in TempData for display at the index view
                TempData["Message"] = "Invalid Login Credentials!";
                // Redirect user back to the index viewthrough an action
                return RedirectToAction("Index");
            }
        }

        public ActionResult StaffMain()
        {
            return View();
        }

        public ActionResult StudentLogin()
        {
            HttpContext.Session.SetString("Role", "Student");
            return RedirectToAction("Index", "Book");
        }

        public ActionResult LogOut()
        {
            // Clear all key-values pairsstored in session state
            HttpContext.Session.Clear();
            // Call the Index action of Home controller
            return RedirectToAction("Index");
        }
    }
}