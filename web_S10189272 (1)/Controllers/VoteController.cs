﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Web_S10189272.Models;

namespace Web_S10189272.Controllers
{
    public class VoteController : Controller
    {
        // GET: Vote
        public ActionResult Index()
        {
            return View();
        }

        // GET: Vote/Details/5
        public async Task<ActionResult> Details(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://ictonejourney.com");
            HttpResponseMessage response = await client.GetAsync("/api/votes/" + id.ToString());
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                List<VoteDetails> voteList = JsonConvert.DeserializeObject<List<VoteDetails>>(data);
                return View(voteList);
            }
            else
            {
                return View(new List<VoteDetails>());
            }
        }

        // GET: Vote/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vote/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(IFormCollection collection)
        {
            int bookid = Convert.ToInt32(collection["item.Id"]);
            string justification = collection["item.Justification"];

            Vote vote = new Vote();
            vote.BookId = bookid;
            vote.Justification = justification;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://ictonejourney.com");
            HttpResponseMessage response = await client.PostAsJsonAsync("/api/votes", vote);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                return RedirectToAction("Details", new { id = bookid });
            }
            else
            {
                TempData["BookId"] = bookid;
                TempData["Justification"] = justification;
                TempData["Message"] = "Fail to add vote record!";   
                
                return RedirectToAction("Index", "Book");     }
            }

        // GET: Vote/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Vote/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Vote/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Vote/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}